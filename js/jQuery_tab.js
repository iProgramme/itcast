/**
 * Created by yubowen on 2017/4/7.
 */
/**
 * tab切换
 * @param ele 切换的div
 * @param source 切换的class
 */
$.fn.tab = function (ele,source) {
    $(this).on("mouseenter", function () {
        console.log($(this))
        $(this).addClass(source).siblings("a").removeClass(source)
        var p = $(this).index() / 2;
        $(ele).eq(p).show().siblings(ele).hide()
    })
}