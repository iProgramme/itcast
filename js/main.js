/**
 * Created by yubowen on 2017/4/7.
 */
$(function () {



    // 全国高校
    $(".nav_guo").on("mouseenter", function () {
        $(".nav_school").removeClass("disN")
    }).on("mouseleave", function () {
        $(".nav_school").addClass("disN")
    })
    // 导航栏下拉菜单
    $(".free_fuceng").on("mouseenter", function () {
        $(".free_things").stop().slideDown(500)
    })
    $("nav").on("mouseleave", function () {
        $(".free_things").stop().slideUp(500)
    })

    // banner左边的
    $(".banner_left li").on("mouseenter", function () {
        $(".banner_nav_info").stop().fadeIn(500)
    })
    $(".banner_left").on("mouseleave", function () {
        $(".banner_nav_info").stop().fadeOut(500)
    })

    //课程中心的小效果
    $(".area_now").on("mouseenter", function () {
        $(".color_bg").stop().animate({bottom: 30}, 200).siblings().css("color", "#000")
        $(this).css("color", "#fff").prev().stop().animate({bottom: 0}, 200)
    })
    // 鼠标滑过往上提一点
    $(".class_left_ul li").on("mouseenter", function () {
        $(".each_class li").stop().animate({top: 0}, 400).css({"box-shadow": ""})
        $(this).stop().animate({top: -5}, 500).css({"box-shadow": "5px 5px 5px #999"})
    }).on("mouseleave", function () {
        $(".each_class li").stop().animate({top: 0}, 400).css({"box-shadow": ""})
    })


    // 轮播图插件
    $(".banner_img").on("mouseenter", function () {
        $(".banner_btn1,.banner_btn2").stop().fadeIn(1000)
    }).on("mouseleave", function () {
        $(".banner_btn1,.banner_btn2").stop().fadeOut(1000)
    })
    var banner = {
        "ulId": "banner_y",  // 轮播图的ul 的id
        "indexParentId": "indexParent",  // 小圆点的父级的id
        "indexActiveClass": "active",    // 小圆点的class
        "btn_leftId": "banner_btn1",     // 左边按钮
        "btn_rightId": "banner_btn2",    // 右边按钮
        "speed": 5000   //轮播的时间间隔
    }
    $(".banner_y").stop().lunboY(banner);
    // 推荐内容滚动,照样使用轮播插件
    var tuijian = {
        "ulId": "lr_content",
        //没有小圆点
        //没有小圆点
        "btn_rightId": "btn_r",
        "btn_leftId": "btn_l",
        "speed": 5000
    }
    $("#lr_content").children().eq(0).clone().appendTo("#lr_content");
    $(".index_title1 h2").stop().lunboY(tuijian);


    // tab 切换插件
    $(".index_title2 h2 a").tab(".tab","source")
    $(".index_title3 h2 a").tab(".tab1","source")
})