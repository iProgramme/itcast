/**
 * Created by yubowen on 2017/4/7.
 */
// 课程中心
$(function () {
    var colors = ['#3688FF','#27B869','#8BA2E4','#E02C4F','#898989','#F5717C','#F3C048','#4EA3D0','#955EBB'];
    var lesson_all = [
        {
            ti:"JavaEE",
            zi:"第一编程语言<br>我们的第一学科",
            src:"img/java.png"
        },{
            ti:"Android",
            zi:"入门快,成长快<br>移动开发起止高薪",
            src:"img/android.png"
        },{
            ti:"PHP",
            zi:"打造横跨前端+后端+移动端<br>的全能型人才",
            src:"img/php.png"
        },{
            ti:"UI设计",
            zi:"打造会代码的全能设计师",
            src:"img/ui.png"
        },{
            ti:"IOS",
            zi:"做被争抢的IOS开发者",
            src:"img/ios.png"
        },{
            ti:"前端与移动开发",
            zi:"大前端改变世界 我们改变前端",
            src:"img/web.png"
        },{
            zi:"C/C++",
            zi:"培养永不过时的全站型<br>C/C++应用开发工程师",
            src:"img/c.png"
        },{
            ti:"网络营销",
            zi:"不用敲代码,一样拿高薪",
            src:"img/m.png"
        },{
            ti:"人工智能+Python",
            zi:"人工智能与Python<br>带你进入智能时代",
            src:"img/python.png"
        }
    ]
    var arr = [];
    for (var i = 0; i < colors.length; i++) {
        arr.push(
            '<!-- JavaEE -->' +
            '<div class="each_class clear"> ' +
            '<div class="class_left fl" style="background:'+colors[i]+'"> ' +
            '<a href="#"> ' +
            '<h3>'+lesson_all[i].ti+'</h3> ' +
            '<p>'+lesson_all[i].zi+'</p> ' +
            '<img src='+lesson_all[i].src+' alt=""> ' +
            '</a> ' +
            '</div> ' +
            '<ul class="fl class_left_ul"> ' +
            '<li> ' +
            '<a href="#"> ' +
            '<img src="img/icon1.png" alt=""> ' +
            '<span>就业薪资</span> ' +
            '</a> ' +
            '</li> ' +
            '<li> ' +
            '<a href="#"> ' +
            '<img src="img/icon2.png" alt=""> ' +
            '<span>班级活动</span> ' +
            '</a> ' +
            '</li> ' +
            '<li> ' +
            '<a href="#"> ' +
            '<img src="img/icon3.png" alt=""> ' +
            '<span>公开课</span> ' +
            '</a> ' +
            '</li> ' +
            '<li> ' +
            '<a href="#"> ' +
            '<img src="img/icon4_1.png" alt=""> ' +
            '<span>视频下载</span> ' +
            '</a> ' +
            '</li> ' +
            '</ul> ' +
            '<ul class="fl class_left_ul"> ' +
            '<li> ' +
            '<a href="#"> ' +
            '<img src="img/icon5.png" alt=""> ' +
            '<span>就业薪资</span> ' +
            '</a> ' +
            '</li> ' +
            '<li> ' +
            '<a href="#"> ' +
            '<img src="img/icon6.png" alt=""> ' +
            '<span>班级活动</span> ' +
            '</a> ' +
            '</li> ' +
            '<li> ' +
            '<a href="#"> ' +
            '<img src="img/icon7.png" alt=""> ' +
            '<span>公开课</span> ' +
            '</a> ' +
            '</li> ' +
            '<li> ' +
            '<a href="#"> ' +
            '<img src="img/icon8.png" alt=""> ' +
            '<span>视频下载</span> ' +
            '</a> ' +
            '</li> ' +
            '</ul> ' +
            '<ul class="class_area fl" style="border-color:'+colors[i]+'"> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">北京</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">上海</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">广州</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">深圳</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">武汉</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">郑州</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">西安</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">哈尔滨</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">长沙</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">济南</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">重庆</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">南京</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">杭州</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">成都</span></li> ' +
            '<li><span class="color_bg" style="background:'+colors[i]+'"></span><span class="area_now">石家庄</span></li> ' +
            '</ul> ' +
            '<div class="jichu fl"> ' +
            '<a href="#"><span>基</span>JavaEE 基础班 &nbsp; 2017-04-08 &nbsp; 火爆报名中</a> ' +
            '<a href="#"><span>基</span>JavaEE 基础班 &nbsp; 2017-04-09 &nbsp; 火爆报名中</a> ' +
            '<a href="#"><span>基</span>JavaEE 基础班 &nbsp; 2017-04-10 &nbsp; 火爆报名中</a> ' +
            '<a href="#"><span>基</span>JavaEE 基础班 &nbsp; 2017-04-11 &nbsp; 火爆报名中</a> ' +
            '</div> ' +
            '<div class="jichu jiuye fl"> ' +
            '<a href="#"><span>就</span>JavaEE 就业班 &nbsp; 2017-04-08 &nbsp; 火爆报名中</a> ' +
            '<a href="#"><span>就</span>JavaEE 就业班 &nbsp; 2017-04-09 &nbsp; 火爆报名中</a> ' +
            '<a href="#"><span>就</span>JavaEE 就业班 &nbsp; 2017-04-10 &nbsp; 火爆报名中</a> ' +
            '<a href="#"><span>就</span>JavaEE 就业班 &nbsp; 2017-04-11 &nbsp; 火爆报名中</a> ' +
            '</div> '+'</div>');
    }
    console.log(arr)

    $(".class_center .w").append(arr.join(''))

})

